MXMLC = mxmlc
FLIXEL = ~/projects/gamedev/flixel
SRC_DIR = src
MAIN = src/com/rhburrows/CombatCube.as
SWF = CombatCube.swf

FILES = src/com/rhburrows/MenuState.as \
	src/com/rhburrows/PlayState.as \
	src/com/rhburrows/Player.as \
	src/com/rhburrows/Arena.as \
	src/com/rhburrows/ArenaWall.as \
	src/com/rhburrows/ArenaCube.as \
	src/com/rhburrows/ArenaOrient.as \
	src/com/rhburrows/HealthBar.as \
	src/com/rhburrows/GameOverState.as \
	src/com/rhburrows/ai/EnemyAI.as \
	src/com/rhburrows/ai/Coward.as \
	src/com/rhburrows/ai/Charger.as

$(SWF): $(MAIN) $(FILES)
	$(MXMLC) -sp $(FLIXEL) -compiler.source-path $(SRC_DIR) -o $(SWF) -- $(MAIN)

run : $(SWF)
	flashplayer $(SWF)

.PHONY: clean
clean:
	rm $(SWF)
