package com.rhburrows {

import org.flixel.*;

public class Arena extends FlxSprite {

  public static const WIDTH : Number = 400;
  public static const HEIGHT : Number = 400;

  private static const WALL_WIDTH  : Number = 7;
  private static const WALL_LENGTH : Number = WIDTH / 3 + 5;

  private static const COLORS : Array = [ 0xffffdddd,
                                          0xffddffdd,
                                          0xffddddff,
                                          0xffffffdd,
                                          0xffddffff,
                                          0xffffddff ];

  public var walls : FlxGroup;
  public var id : int;

  public function Arena(x : Number, y : Number, id : int) {
    super(x, y);
    this.id = id;
    var color : uint = COLORS[id]
    makeGraphic(WIDTH, HEIGHT, color);

    walls = new FlxGroup(8);
    
    //top
    walls.add(new ArenaWall(x, y, WALL_LENGTH, WALL_WIDTH, "top"));
    walls.add(new ArenaWall(x + WIDTH - WALL_LENGTH, y, WALL_LENGTH, WALL_WIDTH, "top"));

    //right
    walls.add(new ArenaWall(x + WIDTH - WALL_WIDTH, y, WALL_WIDTH, WALL_LENGTH, "right"));
    walls.add(new ArenaWall(x + WIDTH - WALL_WIDTH, y + HEIGHT - WALL_LENGTH, WALL_WIDTH, WALL_LENGTH, "right"));

    //bottom
    walls.add(new ArenaWall(x + WIDTH - WALL_LENGTH, y + HEIGHT - WALL_WIDTH, WALL_LENGTH, WALL_WIDTH, "bottom"));
    walls.add(new ArenaWall(x, y + HEIGHT - WALL_WIDTH, WALL_LENGTH, WALL_WIDTH, "bottom"));

    //left
    walls.add(new ArenaWall(x, y + HEIGHT - WALL_LENGTH, WALL_WIDTH, WALL_LENGTH, "left"));
    walls.add(new ArenaWall(x, y, WALL_WIDTH, WALL_LENGTH, "left"));
  }
}

}