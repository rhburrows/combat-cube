package com.rhburrows {

import org.flixel.*;
import flash.utils.Dictionary;

public class ArenaCube {
  public var arenas         : Array;
  public var arenaGroups    : Dictionary;
  public var focus          : Player;
  public var players        : Dictionary;

  private var orientation  : String;
  private var current      : Arena;

  public function ArenaCube() {
    players = new Dictionary();

    arenas = new Array(6);
    arenaGroups = new Dictionary();
    for (var i : Number = 0; i < 6; i++) {
      var arena : Arena = new Arena(110, 40, i);
      arenas[i] = arena;
      arenaGroups[arena] = new FlxGroup();
      arenaGroups[arena].add(arena);
      arenaGroups[arena].add(arena.walls);
      arenaGroups[arena].visible = false;
    }

    var state : ArenaOrient = new ArenaOrient();
    state.arena = arenas[0];
    state.orientation = "uz";
    updateCurrent(state);
  }

  public function addPlayer(player : Player, arena : Arena = null, dir : String = null) : void {
    if (arena == null) {
      arena = current;
    }

    if (dir == null) {
      dir = orientation;
    }

    var state : ArenaOrient = new ArenaOrient();
    state.arena = arena;
    state.orientation = dir;
    players[player] = state;
    arenaGroups[arena].add(player);
  }

  public function update() : void {
    for (var a : Object in arenaGroups) {
      var group : FlxGroup = arenaGroups[a];
      FlxG.collide(group, group);
    }

    for (var p : Object in players) {
      var player : Player = Player(p);
      var arena : Arena = players[player].arena;
      player.gameUpdate();

      if (player.x < arena.x) {
        moveLeft(player);
      } else if (player.x > arena.x + Arena.WIDTH - Player.WIDTH) {
        moveRight(player);
      } else if (player.y < arena.y) {
        moveUp(player);
      } else if (player.y > arena.y + Arena.HEIGHT - Player.HEIGHT) {
        moveDown(player);
      }
      var newArena : Arena = players[player].arena;

      if (newArena != arena) {
        arenaGroups[arena].remove(player);
        arenaGroups[newArena].add(player);
      }
    }
  }

  public function findPlayersByArena(arena : Arena) : Array {
    var ps : Array = new Array();
    for (var p : Object in players) {
      if (players[p].arena == arena) {
        ps.push(p);
      }
    }

    return ps;
  }

  public function moveLeft(player : Player) : void {
    var newState : ArenaOrient = leftOf(players[player]);
    if (player === focus) {
      updateCurrent(newState);
    }
    players[player] = newState;
    player.x = newState.arena.x + Arena.WIDTH - Player.WIDTH - 5;
  }

  public function moveRight(player : Player) : void {
    var newState : ArenaOrient = rightOf(players[player]);
    if (player === focus) {
      updateCurrent(newState);
    }
    players[player] = newState;
    player.x = newState.arena.x + 5;
  }

  public function moveUp(player : Player) : void {
    var newState : ArenaOrient = upFrom(players[player]);
    if (player === focus) {
      updateCurrent(newState);
    }
    players[player] = newState;
    player.y = newState.arena.y + Arena.HEIGHT - Player.HEIGHT - 5;
  }

  public function moveDown(player : Player) : void {
    var newState : ArenaOrient = downFrom(players[player]);
    if (player === focus) {
      updateCurrent(newState);
    }
    players[player] = newState;
    player.y = newState.arena.y + 5;
  }

  private function updateCurrent(state : ArenaOrient) : void {
    var newCurrent : Arena = state.arena;
    if (newCurrent != current) {
      if (current != null) {
        arenaGroups[current].visible = false;
      }
      arenaGroups[newCurrent].visible = true;
    }
    current = newCurrent;
    orientation = state.orientation;
  }

  // The below functions show how the arenas are stiched together into a cube
  // Serious stay away from this code unless there's a bug -- its ugly
  private function leftOf(old : ArenaOrient) : ArenaOrient {
    var newState : ArenaOrient = new ArenaOrient();
    newState.orientation = old.orientation;

    var dir : String;
    if (old.arena == current) {
      dir = orientation;
    } else {
      dir = old.orientation
    }

    switch (dir) {
    case "uz":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[2];
        return newState;
      case 1:
        newState.arena = arenas[0];
        return newState;
      case 2:
        newState.arena = arenas[5];
        return newState;
      case 5:
        newState.arena = arenas[1];
        return newState;
      }

    case "dz":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[1];
        return newState;
      case 1:
        newState.arena = arenas[5];
        return newState;
      case 2:
        newState.arena = arenas[0];
        return newState;
      case 5:
        newState.arena = arenas[2];
        return newState;
      }

    case "uy":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[3];
        return newState;
      case 3:
        newState.arena = arenas[5];
        return newState;
      case 4:
        newState.arena = arenas[0];
        return newState;
      case 5:
        newState.arena = arenas[4];
        return newState;
      }

    case "dy":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[4];
        return newState;
      case 3:
        newState.arena = arenas[0];
        return newState;
      case 4:
        newState.arena = arenas[5];
        return newState;
      case 5:
        newState.arena = arenas[3];
        return newState;
      }

    case "ux":
      switch(old.arena.id) {
      case 1:
        newState.arena = arenas[4];
        return newState;
      case 2:
        newState.arena = arenas[3];
        return newState;
      case 3:
        newState.arena = arenas[1];
        return newState;
      case 4:
        newState.arena = arenas[2];
        return newState;
      }

    case "dx":
      switch(old.arena.id) {
      case 1:
        newState.arena = arenas[3];
        return newState;
      case 2:
        newState.arena = arenas[4];
        return newState;
      case 3:
        newState.arena = arenas[2];
        return newState;
      case 4:
        newState.arena = arenas[1];
        return newState;
      }
    }

    return null;
  }

private function rightOf(old : ArenaOrient) : ArenaOrient {
    var newState : ArenaOrient = new ArenaOrient();
    newState.orientation = old.orientation;

    var dir : String;
    if (old.arena == current) {
      dir = orientation;
    } else {
      dir = old.orientation;
    }

    switch (dir) {
    case "dz":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[2];
        return newState;
      case 1:
        newState.arena = arenas[0];
        return newState;
      case 2:
        newState.arena = arenas[5];
        return newState;
      case 5:
        newState.arena = arenas[1];
        return newState;
      }

    case "uz":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[1];
        return newState;
      case 1:
        newState.arena = arenas[5];
        return newState;
      case 2:
        newState.arena = arenas[0];
        return newState;
      case 5:
        newState.arena = arenas[2];
        return newState;
      }

    case "dy":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[3];
        return newState;
      case 3:
        newState.arena = arenas[5];
        return newState;
      case 4:
        newState.arena = arenas[0];
        return newState;
      case 5:
        newState.arena = arenas[4];
        return newState;
      }

    case "uy":
      switch(old.arena.id) {
      case 0:
        newState.arena = arenas[4];
        return newState;
      case 3:
        newState.arena = arenas[0];
        return newState;
      case 4:
        newState.arena = arenas[5];
        return newState;
      case 5:
        newState.arena = arenas[3];
        return newState;
      }

    case "dx":
      switch(old.arena.id) {
      case 1:
        newState.arena = arenas[4];
        return newState;
      case 2:
        newState.arena = arenas[3];
        return newState;
      case 3:
        newState.arena = arenas[1];
        return newState;
      case 4:
        newState.arena = arenas[2];
        return newState;
      }

    case "ux":
      switch(old.arena.id) {
      case 1:
        newState.arena = arenas[3];
        return newState;
      case 2:
        newState.arena = arenas[4];
        return newState;
      case 3:
        newState.arena = arenas[2];
        return newState;
      case 4:
        newState.arena = arenas[1];
        return newState;
      }
    }

    return null;
  }

  public function upFrom(old : ArenaOrient) : ArenaOrient {
    var newState : ArenaOrient = new ArenaOrient();

    var dir : String;
    if (old.arena == current) {
      dir = orientation;
    } else {
      dir = old.orientation;
    }

    switch(dir) {
    case "uz":
      newState.arena = arenas[3];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "dx";
        return newState;
      case 1:
        newState.orientation = "dy";
        return newState;
      case 2:
        newState.orientation = "uy";
        return newState;
      case 5:
        newState.orientation = "ux";
        return newState;
      }

    case "dz":
      newState.arena = arenas[4];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "dx"
        return newState;
      case 1:
        newState.orientation = "dy";
        return newState;
      case 2:
        newState.orientation = "uy";
        return newState;
      case 5:
        newState.orientation = "ux";
        return newState;
      }

    case "uy":
      newState.arena = arenas[1];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "dx";
        return newState;
      case 3:
        newState.orientation = "dz";
        return newState;
      case 4:
        newState.orientation = "uz";
        return newState;
      case 5:
        newState.orientation = "ux";
        return newState;
      }

    case "dy":
      newState.arena = arenas[2];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "dx";
        return newState;
      case 3:
        newState.orientation = "dz";
        return newState;
      case 4:
        newState.orientation = "uz";
        return newState;
      case 5:
        newState.orientation = "ux";
        return newState;
      }

    case "ux":
      newState.arena = arenas[0];
      switch(old.arena.id) {
      case 1:
        newState.orientation = "dy";
        return newState;
      case 2:
        newState.orientation = "uy";
        return newState;
      case 3:
        newState.orientation = "dz";
        return newState;
      case 4:
        newState.orientation = "uz";
        return newState;
      }

    case "dx":
      newState.arena = arenas[5];
      switch(old.arena.id) {
      case 1:
        newState.orientation = "dy";
        return newState;
      case 2:
        newState.orientation = "uy";
        return newState;
      case 3:
        newState.orientation = "dz";
        return newState;
      case 4:
        newState.orientation = "uz";
        return newState;
      }
    }

    return null;
  }

  public function downFrom(old : ArenaOrient) : ArenaOrient {
    var newState : ArenaOrient = new ArenaOrient();

    var dir : String;
    if (old.arena == current) {
      dir = orientation;
    } else {
      dir = old.orientation;
    }

    switch(dir) {
    case "uz":
      newState.arena = arenas[4];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "ux";
        return newState;
      case 1:
        newState.orientation = "uy";
        return newState;
      case 2:
        newState.orientation = "dy";
        return newState;
      case 5:
        newState.orientation = "dx";
        return newState;
      }

    case "dz":
      newState.arena = arenas[3];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "ux"
        return newState;
      case 1:
        newState.orientation = "uy";
        return newState;
      case 2:
        newState.orientation = "dy";
        return newState;
      case 5:
        newState.orientation = "dx";
        return newState;
      }

    case "uy":
      newState.arena = arenas[2];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "ux";
        return newState;
      case 3:
        newState.orientation = "uz";
        return newState;
      case 4:
        newState.orientation = "dz";
        return newState;
      case 5:
        newState.orientation = "dx";
        return newState;
      }

    case "dy":
      newState.arena = arenas[1];
      switch(old.arena.id) {
      case 0:
        newState.orientation = "ux";
        return newState;
      case 3:
        newState.orientation = "uz";
        return newState;
      case 4:
        newState.orientation = "dz";
        return newState;
      case 5:
        newState.orientation = "dx";
        return newState;
      }

    case "ux":
      newState.arena = arenas[5];
      switch(old.arena.id) {
      case 1:
        newState.orientation = "uy";
        return newState;
      case 2:
        newState.orientation = "dy";
        return newState;
      case 3:
        newState.orientation = "uz";
        return newState;
      case 4:
        newState.orientation = "dz";
        return newState;
      }

    case "dx":
      newState.arena = arenas[0];
      switch(old.arena.id) {
      case 1:
        newState.orientation = "uy";
        return newState;
      case 2:
        newState.orientation = "dy";
        return newState;
      case 3:
        newState.orientation = "uz";
        return newState;
      case 4:
        newState.orientation = "dz";
        return newState;
      }
    }

    return null;
  }
}

}