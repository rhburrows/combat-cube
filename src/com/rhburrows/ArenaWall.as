package com.rhburrows {

import org.flixel.*;

public class ArenaWall extends FlxSprite {

  [Embed(source="data/wallTop.png")]
  protected var wallTop : Class;
  [Embed(source="data/wallBottom.png")]
  protected var wallBottom : Class;
  [Embed(source="data/wallLeft.png")]
  protected var wallLeft : Class;
  [Embed(source="data/wallRight.png")]
  protected var wallRight : Class;


  public function ArenaWall(x : Number, y : Number, width : Number, height : Number, orientation : String) {
    super(x, y);

    if (orientation == "top") {
      loadGraphic(wallTop);
    } else if (orientation == "bottom") {
      loadGraphic(wallBottom);
    } else if (orientation == "left") {
      loadGraphic(wallLeft);
    } else if (orientation == "right") {
      loadGraphic(wallRight);
    }

    immovable = true;
  }
}

}