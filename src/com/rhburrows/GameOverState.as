package com.rhburrows {

import org.flixel.*;

public class GameOverState extends FlxState {

  private var remaining : int;

  private static const PLACES : Array = [ "",
                                          "2nd",
                                          "3rd",
                                          "4th",
                                          "5th",
                                          "6th" ];

  public function GameOverState(remainingEnemies : int) {
    super();
    remaining = remainingEnemies;
  }

  override public function create() : void {
    add(new FlxText(80, 200, 400, getMessage()));
    add(new FlxText(80, 250, 400, "Click to play again."));
  }

  override public function update() : void {
    if (FlxG.mouse.justPressed()) {
      FlxG.switchState(new PlayState);
    }
    super.update();
  }

  private function getMessage() : String {
    if (remaining == 0) {
      return "Congratulations, you were the final survivor!";
    } else {
      var place : String = PLACES[remaining];
      return "You got " + place + " place.";
    }
  }
}

}