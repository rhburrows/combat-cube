package com.rhburrows {

import org.flixel.*;

public class HealthBar extends FlxSprite {

  public static const WIDTH  : int = 100;
  public static const HEIGHT : int = 30;

  public var deadText : FlxText;

  private var player : Player;

  public function HealthBar(x : Number, y : Number, p : Player) : void {
    super(x, y);
    player = p;

    makeGraphic(WIDTH, HEIGHT, 0xffdddddd);
    deadText = new FlxText(x + 30, y + 10, WIDTH, "DEAD");
    deadText.visible = false;
  }

  override public function update() : void {
    super.update();

    fill(0xffdddddd);
    var pos : int = 4;
    for (var i : int = 0; i < player.health; i++) {
      drawLine(pos, 4, pos, 22, 0xff11aa11, 6);
      pos = pos + 8;
    }

    if (player.health <= 0) {
      fill(0xff551111);
      deadText.visible = true;
    }
  }
}

}