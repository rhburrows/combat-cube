package com.rhburrows {

import org.flixel.*;

public class MenuState extends FlxState {

  [Embed(source="data/menu.png")]
  protected var backgroundImg : Class;

  override public function create() : void {
    var background : FlxSprite = new FlxSprite(0, 0);
    background.loadGraphic(backgroundImg);
    add(background);

    add(new FlxText(175, 400, 300, "The goal is to destroy the other combatant. Your shield protects you from the front but you are vulnerable from behind. Use WASD to move, good luck!"));
  }

  override public function update() : void {
    if (FlxG.mouse.justPressed()) {
      FlxG.switchState(new PlayState);
      }
    super.update();
  }
}

}