package com.rhburrows {

import com.rhburrows.ai.*;

import org.flixel.*;
import flash.utils.*;

public class PlayState extends FlxState {

  private static const ENEMY_HEALTH : int = 10;
  private static const ENEMY_AI : Array = [ Charger,
                                            Coward ];

  private var player : Player;
  private var enemies : Array;
  private var world  : ArenaCube;

  private var playerHealth : HealthBar;
  private var enemyHealth : HealthBar;

  private var enemyAIs : Array;

  private var lastHealth : Number;

  private var endStarted : Boolean;

  [Embed(source="data/death.mp3")]
  protected var deathSound : Class;

  override public function create() : void {
    world = new ArenaCube();

    player = new Player(FlxG.width / 2 - 5, FlxG.height / 2 - 16);
    world.addPlayer(player);
    world.focus = player;
    var playerLabel : FlxText = new FlxText(5, 5, 100, "Player");
    add(playerLabel);
    playerHealth = new HealthBar(5, 25, player);
    add(playerHealth);
    add(playerHealth.deadText);

    enemies = new Array();
    enemyAIs = new Array();
    makeEnemies(world);

    for (var a : Object in world.arenaGroups) {
      var group : FlxGroup = world.arenaGroups[a];
      add(group);
    }

    lastHealth = player.health;
    endStarted = false;
  }

  override public function update() : void {
    if (endStarted) {
      return;
    }

    var livingEnemies : int = 0;
    for (var i : int = 0; i < enemies.length; i++) {
      if (enemies[i].alive) {
        livingEnemies++;
      }
    }

    if (!player.alive && livingEnemies == 0) {
      endStarted = true;
      FlxG.fade(0xff555555, 1);
      // Tie goes to the player
      setTimeout(function() : void {
          FlxG.switchState(new GameOverState(0));
        }, 2000);
    }
    if (!player.alive) {
      endStarted = true;
      FlxG.play(deathSound);
      FlxG.fade(0xffaa1111, 1);
      setTimeout(function() : void {
          FlxG.switchState(new GameOverState(livingEnemies));
        }, 2000);
    }
    if (livingEnemies == 0) {
      endStarted = true;
      FlxG.fade(0xffcccccc, 1);
      setTimeout(function() : void {
          FlxG.switchState(new GameOverState(0));
        }, 2000);
    }

    player.resetAcceleration();
    for (i = 0; i < enemies.length; i++) {
      if (enemies[i].alive) {
        enemies[i].resetAcceleration();
        enemyAIs[i].update();
      }
    }

    if (FlxG.keys.W) {
      player.accelerate(1);
    }

    if (FlxG.keys.S) {
      player.accelerate(-0.5);
    }

    if (FlxG.keys.D) {
      player.rotate(1);
    }

    if (FlxG.keys.A) {
      player.rotate(-1);
    }

    super.update();
    world.update();

    if (player.health < lastHealth) {
      lastHealth = player.health;
      FlxG.flash(0xffaa1111, 0.5);
    }
  }

  private static const DIRECTIONS : Array =
    [ "dz", "uz", "dy", "uy", "dx", "ux" ];
  private static const COLORS : Array = [ 0xff11aa11, 0xffaa1111, 0xffaaaa11,
                                          0xffaa11aa, 0xff11aaaa, 0xffaaaaaa ];
  private function makeEnemies(world : ArenaCube) : void {
    var enemiesCreated : int = 0;

    for (var i : int = 0; i < world.arenas.length; i++) {
      var arena : Arena = world.arenas[i];
      
      if (world.findPlayersByArena(arena).length == 0) {
        var color : uint = COLORS[i];
        var enemy : Player = new Player(FlxG.width / 2 - 5,
                                        FlxG.height / 2 - 16, color, false);
        
        var direction : String = DIRECTIONS[Math.floor(FlxG.random() * 6)];
        world.addPlayer(enemy, arena, direction);
        enemy.health = ENEMY_HEALTH;

        var aiType : Class = ENEMY_AI[Math.floor(FlxG.random() * 2)];
        var enemyAI : EnemyAI = new aiType(enemy, world);
        enemyAIs.push(enemyAI);

        var enemyLabel : FlxText = new FlxText(FlxG.width - HealthBar.WIDTH - 5,
                                               5 + (20 + HealthBar.HEIGHT) * enemiesCreated,
                                               100, "Competitor " + (enemiesCreated + 1));
        add(enemyLabel);
        enemyHealth = new HealthBar(FlxG.width - HealthBar.WIDTH - 5,
                                    25 + (20 + HealthBar.HEIGHT) * enemiesCreated,
                                    enemy);
        add(enemyHealth);
        add(enemyHealth.deadText);
        enemies[enemiesCreated] = enemy;
        enemiesCreated++;
      }
    }
  }
}

}