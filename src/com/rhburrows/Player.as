package com.rhburrows {

import org.flixel.*;

public class Player extends FlxSprite {

  public static const WIDTH : Number = 40;
  public static const HEIGHT : Number = 40;

  public static const DEG_TO_RAD : Number = Math.PI / 180;

  private static const DEFAULT_COLOR : uint = 0xff1b569a;

  private var enableSounds : Boolean;

  [Embed(source="data/player.png")]
  protected var sprites : Class;

  [Embed(source="data/hit.mp3")]
  protected var hitSound : Class;

  [Embed(source="data/collide.mp3")]
  protected var collideSound : Class;

  protected var playingSound : FlxSound;

  public function Player(x : Number, y : Number, color : uint = DEFAULT_COLOR, sounds : Boolean = true) {
    super(x, y);
    health = 10;

    angle = -90;

    enableSounds = sounds;

    loadGraphic(sprites, true, false, 0, 0, true);
    if (color != DEFAULT_COLOR) {
      replaceColor(DEFAULT_COLOR, color);
    }

    addAnimation("walking", [0, 1, 2, 3], 7);
    addAnimation("stopped", [1]);

    maxVelocity.x = 120;
    maxVelocity.y = 120;
    maxAngular = 100;
    drag.x = maxVelocity.x * 3;
    drag.y = maxVelocity.y * 3;
    angularDrag = maxAngular * 2;
  }

  public function gameUpdate() : void {
    if (velocity.x != 0 || velocity.y != 0) {
      play("walking");
    } else {
      play("stopped");
    }

    if (isTouching(backside())) {
      playingSound = FlxG.play(hitSound);
      hurt(2);
      velocity.x = getDirection().x * maxVelocity.x;
      velocity.y = getDirection().y * maxVelocity.y;
    }

    if (isTouching(frontside())) {
      if (enableSounds) {
        FlxG.play(collideSound, 1, false);
      }
      velocity.x = - getDirection().x * maxVelocity.x;
      velocity.y = - getDirection().y * maxVelocity.y;
    }
  }

  override public function kill() : void {
    super.kill();
    if (playingSound != null) {
      playingSound.stop();
    }
  }

  public function accelerate(dir : Number) : void {
    acceleration.x = getDirection().x * maxVelocity.x * 6 * dir;
    acceleration.y = getDirection().y * maxVelocity.y * 6 * dir;
  }

  public function rotate(dir : Number) : void {
    angularAcceleration = maxAngular * 4 * dir;
  }

  public function resetAcceleration() : void {
    acceleration.x = 0;
    acceleration.y = 0;
    angularAcceleration = 0;
  }

  private function backside() : uint {
    var c : Number = Math.cos(angle * DEG_TO_RAD);
    var s : Number = Math.sin(angle * DEG_TO_RAD);

    var dangerZone : uint = 0x0000;
    if (c > 0) {
      dangerZone = dangerZone | 0x0001;
    } else if (c < 0) {
      dangerZone = dangerZone | 0x0010;
    }

    if (s > 0) {
      dangerZone = dangerZone | 0x0100;
    } else if (s < 0) {
      dangerZone = dangerZone | 0x1000;
    }

    return dangerZone;
  }

  private function frontside() : uint {
    var c : Number = Math.cos(angle * DEG_TO_RAD);
    var s : Number = Math.sin(angle * DEG_TO_RAD);

    var safeZone : uint = 0x0000;
    if (c > 0) {
      safeZone = safeZone | 0x0010;
    } else if (c < 0) {
      safeZone = safeZone | 0x0001;
    }

    if (s > 0) {
      safeZone = safeZone | 0x1000;
    } else if (s < 0) {
      safeZone = safeZone | 0x0100;
    }
    return safeZone;
  }

  private var lastAngle : Number;
  private var lastDirection : FlxPoint;
  private function getDirection() : FlxPoint {
    // cache previous lookups
    if (lastAngle == angle) {
      return lastDirection;
    }

    lastAngle = angle;
    lastDirection = new FlxPoint(Math.cos(angle * DEG_TO_RAD),
                                 Math.sin(angle * DEG_TO_RAD));
    return lastDirection;
  }
}

}