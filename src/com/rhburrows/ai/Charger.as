package com.rhburrows.ai {

import com.rhburrows.*;

import org.flixel.*;

public class Charger extends EnemyAI {

  private var target : Player;
  private var lastArena : Arena;
  private var plannedExit : FlxPoint;

  public function Charger(enemy : Player, world : ArenaCube) {
    super(enemy, world);

    lastArena = world.players[enemy].arena;
  }

  override public function update() : void {
    var arena : Arena = world.players[enemy].arena;
    if (arena != lastArena) {
      target = null;
      plannedExit = null;
      lastArena = arena;
    }

    if (target != null &&
        (world.players[target].arena != arena || !target.alive)) {
      target = null;
    }

    if (target == null) {
      target = findTarget();
    }

    if (target == null) {
      if (plannedExit == null) {
        var exitNum : Number = Math.floor(FlxG.random() * 4);
        plannedExit = EXITS[exitNum];
      } else {
        if (!facing(plannedExit)) {
          rotateTo(plannedExit);
        } else {
          enemy.accelerate(1);
        }
      }
    } else {
      var targetPos : FlxPoint = new FlxPoint(target.x, target.y);

      if (!facing(targetPos)) {
        rotateTo(targetPos);
      } else {
        enemy.accelerate(1);
      }
    }
  }

  private function findTarget() : Player {
    var arena : Arena = world.players[enemy].arena;
    var players : Array = world.findPlayersByArena(arena);
    if (players.length > 1) {
      var possibleTargets : Array = new Array();
      for (var i : int = 0; i < players.length; i++) {
        if (players[i] != enemy) {
          possibleTargets.push(players[i]);
        }
      }
      var idx : int = Math.floor(FlxG.random() * possibleTargets.length);
      return possibleTargets[idx];
    }

    return null;
  }
}

}