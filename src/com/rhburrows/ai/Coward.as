package com.rhburrows.ai {

import com.rhburrows.*;

import org.flixel.*;

public class Coward extends EnemyAI{

  private var target : FlxPoint;
  private static const CENTER : FlxPoint = new FlxPoint(310, 240);
  private static const WALK_IN : int = 25;

  private var walkIn : int;

  public function Coward(enemy : Player, world : ArenaCube) {
    super(enemy, world);
  }

  override public function update() : void {
    var arena : Arena = world.players[enemy].arena;
    var players : Array = world.findPlayersByArena(arena);
    var playerCount : int = 0;

    for (var i : int = 0; i < players.length; i++) {
      if (players[i].alive) {
        playerCount++;
      }
    }

    if (playerCount > 1) {
      walkIn = 0;
      if (target == null) {
        target = EXITS[Math.floor(FlxG.random() * 4)];
      } else {
        if (!facing(target)) {
          rotateTo(target);
        } else {
          enemy.accelerate(1);
        }
      }
    } else {
      target = null;

      if (walkIn <= WALK_IN) {
        if (!facing(CENTER)) {
          rotateTo(CENTER);
        } else {
          enemy.accelerate(1);
        }
        walkIn++;
      }
    }
  }
  
}

}