package com.rhburrows.ai {

import com.rhburrows.*;

import org.flixel.*;

public class EnemyAI {

  // Now with MAGIC NUMBERS!
  protected static const EXITS : Array = [ new FlxPoint(310, 40), // up
                                           new FlxPoint(310, 440), // down
                                           new FlxPoint(510, 240), // right
                                           new FlxPoint(110, 240) ]// left

  protected var enemy : Player;
  protected var world : ArenaCube;

  public function EnemyAI(enemy : Player, world : ArenaCube) {
    this.enemy = enemy;
    this.world = world;
  }

  public function update() : void {}

  protected function facing(p : FlxPoint) : Boolean {
    var angle : Number = angleTo(p);
    return angle > (Math.PI - (Math.PI / 16)) || angle < (-Math.PI + (Math.PI / 16));
  }

  protected function rotateTo(p : FlxPoint) : void {
    var angle : Number = angleTo(p);
    enemy.rotate(angle * 2);
  }

  // in radians
  protected function angleTo(p : FlxPoint) : Number {
    var adjustedX : Number = p.x - enemy.x;
    var adjustedY : Number = p.y - enemy.y;

    var adjustedAngle : Number = adjustAngle(enemy.angle * Player.DEG_TO_RAD);
    var goalAngle : Number = Math.atan2(adjustedY, adjustedX);

    return adjustAngle(adjustedAngle - goalAngle + Math.PI);
  }

  // normalize radians between PI and -PI
  private function adjustAngle(angle : Number) : Number {
    var adjustedAngle : Number = angle;
    while(adjustedAngle >= Math.PI) {
      adjustedAngle = adjustedAngle - (2 * Math.PI);
    }

    while(adjustedAngle < -Math.PI) {
      adjustedAngle = adjustedAngle + (2 * Math.PI);
    }

    return adjustedAngle;
  }

}

}
